package co.edu.uvp.pri.animation.ui.animation;

import java.awt.Color;
import java.awt.Graphics;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;

public class Animation extends JComponent implements Runnable {

    private Thread thread = null;
    private final static short BALL_WIDTH= 50;
    // parametros de la bola trambolica
    private int bx = 0;
    private int by = 0;
    private int dx = 2;
    private int dy = 2;
    
    // color
    private Color colorRGB = new Color(97,212,195);

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        // solamente lógica de dibujar 
        g.setColor(colorRGB);
        g.fillOval(bx, by, BALL_WIDTH, BALL_WIDTH);
        
        
        
    }

    @Override
    public void run() {
        while (this.thread != null) {
            // Lógica de la animación - modificar variables
            
            
            // mover a los lados
            if(bx  >= getWidth() - BALL_WIDTH || (bx < 0)){
                dx = -dx;
            }
            bx += dx;
            
            // mover en diagonal
            if(by >= getHeight() - BALL_WIDTH || (by<0)){
                dy = -dy;
            }
            by += -dy; 
            repaint();
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
                Logger.getLogger(Animation.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void init() {
        if (this.thread == null) {
            this.thread = new Thread(this);
            this.thread.start();
        }
    }

    public void pause() {
        this.thread = null;
    }

    public void restart() {
        // Iniciar variables
        this.init();
    }

    public void setThread(Thread thread) {
        this.thread = thread;
    }

    public void setBx(int bx) {
        this.bx = bx;
    }

    public void setBy(int by) {
        this.by = by;
    }

    public void setDx(int dx) {
        this.dx = dx;
    }

    public void setDy(int dy){
        this.dy = dy;
    }
    public void setColorRGB(Color colorRGB) {
        this.colorRGB = colorRGB;
    }
    
    
}
